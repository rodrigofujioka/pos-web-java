# pos-web-java

Disciplina de Java web na especialização em desenvolvimento web da Unipê.

# Requisitos

Java - 1.10.x

JDK or OpenJDK - 1.10.x

Maven - 3.x.x

MySQL - 5.x.x or MariaDB 10.x.x
PostgreSQL - 10.x.x

Spring - 2.0.0.RELEASE (inclued in pom.xml)


*Tested on:*

GNU/Linux Ubuntu 18.04.1

OpenJDK 10.0.2+13

postgresql-10



# instalar a JDK
sudo apt install default-jdk


# instalar o maven
sudo apt-get install maven

# instalar o PostGreSQL
apt-get install postgresql-10

# olhar versão mais recente em https://spring.io/tools  
# O Curso segue com Instalação do SpringTools
wget http://download.springsource.com/release/STS4/4.0.0.RELEASE/dist/e4.9/spring-tool-suite-4-4.0.0.RELEASE-e4.9.0-linux.gtk.x86_64.tar.gz
sudo tar -xvf spring-tool-suite-4-4.0.0.RELEASE-e4.9.0-linux.gtk.x86_64.tar.gz



# Instalação do Visual Studio Code
  Baixar  https://code.visualstudio.com/docs/?dv=linux64_deb
  Navegue até a pasta onde baixou e 
 *sudo dpkg -i code_1.27.2-1536736588_amd64.deb*



# GitLAB
Vamos utilizar o gitlab durante essa disciplina.
https://gitlab.com/help/ssh/README#generating-a-new-ssh-key-pair 

# Instalar o docker https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04

